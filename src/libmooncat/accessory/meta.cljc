(ns libmooncat.accessory.meta
  (:require
   [clojure.string :as str]))

;; 3b reserved | 2b audience | 1b mirror-placement | 1b mirror-accessory | 1b background

(def ^:const +zeros+ "00000000")

(defn- to-bits [l n]
  (let [bits #?(:cljs (.toString n 2)
                :clj (Integer/toString n 2))
        bl (count bits)]
    (cond
      (< l bl) (throw (ex-info "Number too large for bit-length" {:number n
                                                                            :bit-length l}))
      (== l bl) bits

      :else (str (subs +zeros+ 0 (- l bl)) bits))))

(defn- bits->int [& segments]
  #?(:clj (Integer/parseInt (str/join segments) 2)
     :cljs (js/parseInt (str/join segments) 2)))


(defn truthy->int [x]
  (if (or (nil? x) (false? x) (= 0 x))
    0
    1))

(defn to-metabyte [{:keys [background audience mirror-accessory mirror-placement verified] :or {audience 0
                                                                                                category 0}}]
  (let [background (truthy->int background)

        verified (truthy->int verified)
        mirror-accessory (truthy->int mirror-accessory)
        mirror-placement (truthy->int mirror-placement)

        audience (if (and (int? audience) (<= 0 audience) (< audience 4))
                   audience
                   0)]
    (bits->int
     (to-bits 1 verified)
     (to-bits 2 0)
     (to-bits 2 audience)
     (to-bits 1 mirror-placement)
     (to-bits 1 mirror-accessory)
     (to-bits 1 background))))

(defn from-metabyte [n]
  (if (int? n)
    (let [bits (to-bits 8 n)]
      {:verified (= "1" (subs bits 0 1))
       :reserved (bits->int (subs bits 1 3))
       :audience (bits->int (subs bits 3 5))
       :mirror-placement (= "1" (subs bits 5 6))
       :mirror-accessory (= "1" (subs bits 6 7))
       :background (= "1" (subs bits 7))})
    {:verified false
     :reserved 0
     :audience 0
     :mirror-placement false
     :mirror-accessory false
     :background false}))
