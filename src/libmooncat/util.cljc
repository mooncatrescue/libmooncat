(ns libmooncat.util
  (:require
   [clojure.string :as str])
  #?(:clj
     (:import
      [java.util Base64])))

;;; general

(defn floor [x]
  #?(:clj (long (Math/floor x))
     :cljs (js/Math.floor x)))

(defn abs [x]
  #?(:clj (Math/abs x)
     :cljs (js/Math.abs x)))

(defn round [x]
  #?(:clj (if (integer? x) x (long (Math/round x)))
     :cljs (js/Math.round x)))

(defn parse-int [x]
  #?(:clj (Integer/parseInt (str x))
     :cljs (js/parseInt (str x) 10)))

;;; hex

(defn clean-hex-prefix [s]
  (if (str/starts-with? s "0x")
    (subs s 2)
    s))

(defn add-hex-prefix [s]
  (if (str/starts-with? s "0x")
    s
    (str "0x" s)))

(defn byte->hex [i]
  (let [i (long i)
        i (if (neg? i) (+ i 256) i)
        s #?(:clj  (Integer/toString i 16)
             :cljs (.toString i 16))]
    (if (= 1 (count s))
      (str "0" s)
      s)))

(defn hex->int [s]
  #?(:cljs (js/parseInt s 16)
     :clj (Integer/parseInt s 16)))

;;; bin

(defn byte->bin [i]
  (let [bin #?(:clj (Integer/toString i 2)
               :cljs (.toString i 2))]
    (str (subs "00000000" (count bin)) bin)))

(defn bin->int [s]
  #?(:clj (Integer/parseInt s 2)
     :cljs (js/parseInt s 2)))

(defn- bin->hex-helper [bin]
  #?(:clj (Integer/toString (Integer/parseInt bin 2) 16)
     :cljs (.toString (js/parseInt bin 2) 16)))

(defn bin->hex [binary-string]
  (when-not (zero? (mod (count binary-string) 8))
    (throw (ex-info "binary string length must be a multiple of 8" {:length (count binary-string)
                                                                    :binary-string binary-string})))
  (str/join (map #(bin->hex-helper (str/join %)) (partition 4 binary-string))))

;;; byte-arrays

(defn byte-array->hex
  "Takes a byte array and evaluates to it's hexidecimal representation"
  [ba]
  (str "0x" (str/join (map byte->hex ba))))

(defn hex->byte-array [hex-string]
  (let [s (clean-hex-prefix hex-string)
        byte-vec (mapv hex->int (re-seq #".{2}" s))
        res #?(:clj (byte-array (count byte-vec))
               :cljs (js/Uint8Array. (count byte-vec)))]
    (dotimes [i (count byte-vec)]
      #?(:clj (let [b (byte-vec i)
                    b (if (< 127 b) (- b 256) b)]
                (aset res i (byte b)))
         :cljs (aset res i (byte (byte-vec i)))))
    res))

(defn byte-array->base64 [ba]
  #?(:cljs (js/btoa (str/join (map #(js/String.fromCharCode %) ba)))
     :clj (.encodeToString (Base64/getEncoder) ^bytes ba)))

;;; Keywords

(defn camel-case-keyword [kw]
  (let [s (name kw)]
    (str/replace s #"\-(.)" (fn [[_ c]] (str/upper-case c)))))

(defn keywordize-string [s]
  (keyword (str/replace s #"([A-Z])" (fn [[_ c]]
                                       (str "-" (str/lower-case c))))))
